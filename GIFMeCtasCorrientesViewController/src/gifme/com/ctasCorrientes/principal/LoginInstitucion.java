package gifme.com.ctasCorrientes.principal;


import javax.faces.application.NavigationHandler;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.servlet.http.HttpSession;

import oracle.adf.model.BindingContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

public class LoginInstitucion{
    private String idInstitucion;
    private String link;
    private String usuario;
    private String password;

    public LoginInstitucion() { }

    public void iniciarSesion(ActionEvent actionEvent) {
        String idSesion = null;
        BindingContext bctx = new BindingContext();
        BindingContainer bctr = bctx.getCurrentBindingsEntry();
        OperationBinding op = bctr.getOperationBinding("iniciarSesionCuentasCorrientes");
        op.getParamsMap().put("usuario", this.usuario);
        op.getParamsMap().put("pass", this.password);
        op.getParamsMap().put("institucion", this.idInstitucion);
        op.execute();
        idSesion = op.getResult().toString();
        if(idSesion.contains("ERROR")){
            System.out.println("NO TIENE ID SESION");
            return;
        }
        FacesContext fctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = fctx.getExternalContext();
        HttpSession userSession = (HttpSession) ectx.getSession(false);
        userSession.setAttribute("sesionId", idSesion);
        this.cambiarPagina();
    }
    
    public void cambiarPagina() {
        String codigoJs = "document.getElementById('cb1').click();";
        FacesContext context = FacesContext.getCurrentInstance();
        ExtendedRenderKitService erks = Service.getRenderKitService(context, ExtendedRenderKitService.class);
        erks.addScript(context, codigoJs);
    }
    
    public void setIdInstitucion(String idInstitucion) {
        this.idInstitucion = idInstitucion;
    }

    public String getIdInstitucion() {
        return idInstitucion;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLink() {
        return link;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

}
