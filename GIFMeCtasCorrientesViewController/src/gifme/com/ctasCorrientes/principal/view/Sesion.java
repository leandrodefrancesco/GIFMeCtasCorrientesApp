package gifme.com.ctasCorrientes.principal.view;

import javax.faces.component.html.HtmlCommandButton;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import javax.servlet.http.HttpSession;

import nativo.com.view.b_NativoTools;

import oracle.adf.model.BindingContext;
import oracle.adf.view.rich.component.rich.input.RichInputText;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class Sesion {
    private String idSesionNuevo;
    private HtmlCommandButton button;
    private RichInputText idSesionGIFMeIn;
    private String idSesionIn;
    private b_NativoTools bTools = new b_NativoTools();

    public Sesion() {
    }

    public void init(String idSesion) {
        try {
            if (idSesion.length() > 0) {
                BindingContext bctx = new BindingContext();
                BindingContainer bctr = bctx.getCurrentBindingsEntry();
                OperationBinding op = bctr.getOperationBinding("setearIdSesionObtenido");
                op.getParamsMap().put("idSesion", idSesion);
                op.execute();

                FacesContext fctx = FacesContext.getCurrentInstance();
                ExternalContext ectx = fctx.getExternalContext();
                HttpSession userSession = (HttpSession) ectx.getSession(false);
                userSession.setAttribute("sesionId", idSesion);

                this.cambiarPagina();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void cambiarPagina() {
        String codigoJs = "document.getElementById('cb1').click();";
        bTools.ejecutarJavaScript(codigoJs);
    }

    public void setIdSesionNuevo(String idSesionNuevo) {
        this.idSesionNuevo = idSesionNuevo;
    }

    public String getIdSesionNuevo() {
        return idSesionNuevo;
    }


    public void setButton(HtmlCommandButton button) {
        this.button = button;
    }

    public HtmlCommandButton getButton() {
        return button;
    }

    //eliminar los 4 metodos siguientes finalizados en In
    public void setIdSesionGIFMeIn(RichInputText idSesionGIFMeIn) {
        this.idSesionGIFMeIn = idSesionGIFMeIn;
    }

    public RichInputText getIdSesionGIFMeIn() {
        return idSesionGIFMeIn;
    }

    public void setIdSesionIn(String idSesionIn) {
        this.idSesionIn = idSesionIn;
    }

    public String getIdSesionIn() {
        return idSesionIn;
    }
}
