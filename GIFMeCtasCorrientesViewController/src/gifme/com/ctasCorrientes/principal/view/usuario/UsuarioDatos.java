package gifme.com.ctasCorrientes.principal.view.usuario;

import java.io.Serializable;

import oracle.adf.model.BindingContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class UsuarioDatos implements Serializable {
    private String datosUsuario;

    public UsuarioDatos() {
    }

    public void obtenerDatos(String idSesion) {
        BindingContext bctx = new BindingContext();
        BindingContainer bctr = bctx.getCurrentBindingsEntry();
        OperationBinding op = bctr.getOperationBinding("obtenerUsuario");
        op.getParamsMap().put("idSesion", idSesion);
        op.getParamsMap().put("operacion", 1);
        op.execute();
        datosUsuario = (String) op.getResult();
    }

    public void setDatosUsuario(String datosUsuario) {
        this.datosUsuario = datosUsuario;
    }

    public String getDatosUsuario() {
        return datosUsuario;
    }
}
