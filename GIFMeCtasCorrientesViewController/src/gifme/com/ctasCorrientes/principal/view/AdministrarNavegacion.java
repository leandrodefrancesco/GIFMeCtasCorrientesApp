package gifme.com.ctasCorrientes.principal.view;


import java.io.Serializable;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import javax.servlet.http.HttpSession;

import nativo.com.view.b_NativoTools;

import oracle.adf.model.BindingContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


public class AdministrarNavegacion implements Serializable {
    private String pagina;
    private b_NativoTools bTools = new b_NativoTools();
    private String urlRetorno;


    public AdministrarNavegacion() {
    }

    public void setPaginaParaCambio(String paginaParaIngresar, String idSesion) {
        try {
            if (idSesion.length() > 0) {
                BindingContext bctx = new BindingContext();
                BindingContainer bctr = bctx.getCurrentBindingsEntry();
                OperationBinding op = bctr.getOperationBinding("setearIdSesionObtenido");
                op.getParamsMap().put("idSesion", idSesion);
                op.execute();

                FacesContext fctx = FacesContext.getCurrentInstance();
                ExternalContext ectx = fctx.getExternalContext();
                HttpSession userSession = (HttpSession) ectx.getSession(false);
                userSession.setAttribute("sesionId", idSesion);
                this.pagina = paginaParaIngresar;
                String codigoJs = "document.getElementById('cb1').click();";
                bTools.ejecutarJavaScript(codigoJs);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public String LlamadoNavegacion(String Accion) {
        System.out.println("LA ACCION ES: " + Accion);
        this.pagina = Accion;
        
        
        return this.pagina;
    }

    public String LlamadoResetear() {
        return this.pagina;
    }

    public void setPagina(String pagina) {
        this.pagina = pagina;
    }

    public String getPagina() {
        return pagina;
    }

    public void irPaginaPorJavascript(String nombrePagina) {
        String codigoJs="window.open('"+nombrePagina+"', '_self');";
        if(nombrePagina.contains("-")){
            String modulo = null;
            String paginaParaIr = nombrePagina.substring(nombrePagina.lastIndexOf("-") + 1);
            String idSesion = this.bTools.resolvElDC("#{sessionScope.sesionId}").toString();
            if(nombrePagina.contains("Principal")){
                modulo = "GIFMe";
            }
            codigoJs="window.open('/"+modulo+"/faces/goPaginaGIFMe?nombrePagina="+paginaParaIr+"&iS="+idSesion+"', '_self');";
        }
        this.bTools.ejecutarJavaScript(codigoJs);
    }

    public void generarEnlaceDeRetorno(String idSesion) {
        String urlRetorno = "/GIFMe/faces/returnGIFMe?iS=" + idSesion;
        String codigoJs="window.open('"+urlRetorno+"', '_self');";
        this.bTools.ejecutarJavaScript(codigoJs);
    }
}
