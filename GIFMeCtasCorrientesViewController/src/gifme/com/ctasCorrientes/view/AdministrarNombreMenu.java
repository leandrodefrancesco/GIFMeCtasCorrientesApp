package gifme.com.ctasCorrientes.view;

import gifme.com.ctasCorrientes.principal.view.AdministrarNavegacion;

import oracle.adf.model.BindingContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class AdministrarNombreMenu extends AdministrarNavegacion {
    private String prueba;
    private String urlRetorno;
    private String nombrePagina;

    public AdministrarNombreMenu() {
    }

    public String analizarAccion() {
        return null;
    }

    public void generarEnlaceDeRetorno(String idSesion) {
        this.setUrlRetorno("/GIFMe/faces/returnGIFMe?iS=" + idSesion);
    }

    public void setPrueba(String prueba) {
        this.prueba = prueba;
    }

    public String getPrueba() {
        return prueba;
    }

    public void setUrlRetorno(String urlRetorno) {
        this.urlRetorno = urlRetorno;
    }

    public String getUrlRetorno() {
        return urlRetorno;
    }

    public void setNombrePagina(String nombrePagina) {
        this.nombrePagina = nombrePagina;
    }

    public String getNombrePagina() {
        return nombrePagina;
    }
}
